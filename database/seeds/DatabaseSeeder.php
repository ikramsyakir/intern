<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // calling seeder classes
        $this->call(UserSeeder::class);
        $this->call(ListingSeeder::class);
    }
}
