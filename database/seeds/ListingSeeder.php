<?php

use App\Listing;
use Illuminate\Database\Seeder;

class ListingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create listing data
        Listing::create([
            'list_name' => 'Starbucks @ Mid Valley Megamall',
            'address' => 'Lingkaran Syed Putra, Mid Valley City',
            'latitude' => '3.117880',
            'longitude' => '101.676749',
            'submitter_id' => 2
        ]);

        Listing::create([
            'list_name' => 'Ticklish Ribs & Wiches',
            'address' => 'REXKL 80, Kuala Lumpur',
            'latitude' => '3.1398266',
            'longitude' => '101.6885936',
            'submitter_id' => 2
        ]);

        Listing::create([
            'list_name' => 'myBurgerLab Sunway',
            'address' => 'Bandar Sunway, Selangor',
            'latitude' => '3.068672',
            'longitude' => '101.6009317',
            'submitter_id' => 2
        ]);

        Listing::create([
            'list_name' => 'PappaRich',
            'address' => 'Sunway Pyramid Shopping Mall, Selangor',
            'latitude' => '3.06872',
            'longitude' => '101.5856108',
            'submitter_id' => 2
        ]);
    }
}
