<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create user
        User::create([
            'id' => 2,
            'name' => 'Administrator',
            'email' => 'admin@admin.com',
            'type' => 'a',
            'password' => '$2y$10$82R2HLAFN7IHhacd4qUKS.dUYYS/ikBiIVF0tGB2IK3uZsEfLdlx2', // 123456
        ]);
    }
}
