<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){ // check user auth
            if(Auth::user()->isAdmin()){ // check user isAdmin
                return $next($request);
            }
        }

        return redirect('/home')->with('danger', 'You are not authorized!'); // return with flash message
    }
}
