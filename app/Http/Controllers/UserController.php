<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // show all user
        return view('user.index')->with('users', User::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // show form create user
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate user input
        Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'type' => 'required',
            'password' => 'required|confirmed|min:6'
        ])->validate();

        // store user data
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'type' => $request->type,
            'password' => Hash::make($request->password) // hash password
        ]);

        // flash message
        session()->flash('success', 'User successfully created!');

        // redirect to user page
        return redirect('/users');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // edit user by ID
        return view('user.edit')->with('user', User::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // get user
        $user = User::findOrFail($id);

        // validate user input
        Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email, '. $id . ',id',
            'type' => 'required',
            'password' => 'nullable|confirmed|min:6'
        ])->validate();

        // if user not fill the password field
        if (trim($request->password) == null){

            // update user
            $user->update([
                'name' => $request->name,
                'email' => $request->email,
                'type' => $request->type
            ]);

            // flash message
            session()->flash('success', 'User successfully updated!');

            // redirect to user page
            return redirect('/users');

        } else {

            // if user fill the password field
            $user->update([
                'name' => $request->name,
                'email' => $request->email,
                'type' => $request->type,
                'password' => $request->password
            ]);

            // flash message
            session()->flash('success', 'User successfully updated!');

            // redirect to user page
            return redirect('/users');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // get user
        $user = User::findOrFail($id);

        // delete user
        $user->delete();

        // flash message
        session()->flash('success', 'User successfully deleted!');

        // redirect to user page
        return redirect('/users');
    }
}
