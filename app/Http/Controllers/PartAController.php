<?php

namespace App\Http\Controllers;

class PartAController extends Controller
{
    public function question1()
    {
        $a = 19; $b = 75; // default values

        // Display value before swap
        echo "Before swap value: a = " . $a . ", " . "b = " . $b;

        // Code to swap 'a' and 'b'
        $a = $a + $b; // a now becomes 94
        $b = $a - $b; // b becomes 19
        $a = $a - $b; // a becomes 75

        // Display value after swap
        echo "<br>After swap value: a = " . $a . ", " . "b = " . $b;
    }

    public function question2()
    {
        $mbps = 100; // declare mbps to 100
        $kbps = 125; // 1 mbps is equal to 125 kB/s

        // calculate the speed mbps to kbps
        $kbps = $mbps * $kbps;

        // result
        echo $kbps . ' kB/s';
    }

    public function question3()
    {
        echo "<ol>
                <li>It is vulnerable to SQL injection which can allow malicious behavior such as add, retrieve, modify and delete records in database.</li>
                <li>Never use user input directly in queries.</li>
                <li>Sanitize and validate it first.</li>
                <li>Preferably use prepared statements (PDO).</li>
              </ol>";
    }

    public function question4()
    {
        echo "Answer: A and E";
    }

    public function question5()
    {
        // array values
        $values = [8, 10, 7, 3, 2, 9, 1, 5, 4, 6];

        // filter odd elements
        $odd = array_filter($values, function ($array) { return $array & 1; });

        // filter even elements
        $even = array_filter($values, function ($array) { return !($array & 1); });

        // show array for odd and event
        // first param = odd, second param = even
        dd($odd, $even);
    }

    public function question6()
    {
        $huge_box = 8; // 8 huge boxes can fit in 1 carton
        $small_box = 10; // 10 huge boxes can fit in 1 carton

        // calculate to get send out boxes
        $calculate = (7 * $huge_box) + (4 * $small_box);

        // display total box
        echo "Total box (huge and small): " . $calculate . " boxes";

        // calculate cartons
        $total_carton = 7 + 4;

        // display total cartons
        echo "<br>Answer total cartons: " . $total_carton . " cartons";
    }

}
