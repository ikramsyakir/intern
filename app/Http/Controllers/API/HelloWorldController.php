<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\HelloWorld;
use Illuminate\Http\Request;

class HelloWorldController extends Controller
{
    public function store(Request $request) {

        // validate user input
        $data = $request->validate([
            'greeting' => 'required',
            'created_by' => 'required'
        ]);

        $hello_world = HelloWorld::create([
            'greeting' => $data['greeting'],
            'created_by' => $data['created_by']
        ]);

        return response()->json(['hello_world' => $hello_world], 200);

    }
}
