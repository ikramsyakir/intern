<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        // validate user input
        $loginData = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        // user login attempt
        if (auth()->attempt(['email'=>$loginData['email'],'password'=>$loginData['password']])) {

            $token = auth()->user()->createToken('API Token')->accessToken; // generate token

            // return the data in json
            return response()->json(['user_id' => auth()->user()->id, 'token' => $token,
                'status' => ['code' => 200, 'message' => 'Access Granted']], 200);

        } else {

            // if user is not authorized
            return response()->json(['status' => [ 'code' => 401, 'message' => 'Unauthorized']], 401);

        }

    }
}
