<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Listing;
use Location\Coordinate;
use Location\Distance\Vincenty;

class ListingController extends Controller
{
    public function getListing($id, $lat, $long)
    {
        $data = []; // declare variable array

        $listings = Listing::whereSubmitterId($id)->get(); // get listing

        if ($listings) { // if listing exist

            foreach ($listings as $listing){

                $coordinate1 = new Coordinate($lat, $long); // from user input
                $coordinate2 = new Coordinate($listing->latitude, $listing->longitude); // from database

                // data to response
                $data[] = ['id' => $listing->id, 'list_name' => $listing->list_name,
                    'distance' => number_format($coordinate1->getDistance($coordinate2, new Vincenty()) / 1000, 3)];

            }

            // return the data in json
            return response()->json(['listing' => $data,
                'status' => ['code' => 200, 'message' => 'Listing successfully retrieved']], 200);

        } else {

            // if user has not listing exist
            return response()->json(['listing' => 'No data for this user',
                'status' => ['code' => 404, 'message' => 'Listing not available']], 404);

        }



    }
}
