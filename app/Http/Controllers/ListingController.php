<?php

namespace App\Http\Controllers;

use App\Listing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // list all listing
        return view('listing.index')->with('listings', Listing::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // show form create listing
        return view('listing.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate user input
        Validator::make($request->all(), [
            'list_name' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required'
        ])->validate();

        // store data in listing model
        Listing::create([
            'list_name' => $request->list_name,
            'address' => $request->address,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'submitter_id' => Auth::user()->id
        ]);

        // flash message
        session()->flash('success', 'Listing successfully created!');

        // redirect to listing page
        return redirect('/listing');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function edit(Listing $listing)
    {
        // edit listing by ID
        return view('listing.edit')->with('listing', Listing::findOrFail($listing->id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Listing $listing)
    {
        // find listing
        $listing = Listing::findOrFail($listing->id);

        // validate user input
        Validator::make($request->all(), [
            'list_name' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required'
        ])->validate();

        // update listing
        $listing->update([
            'list_name' => $request->list_name,
            'address' => $request->address,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'submitter_id' => Auth::user()->id
        ]);

        // flash message
        session()->flash('success', 'Listing successfully updated!');

        // redirect to listing page
        return redirect('/listing');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function destroy(Listing $listing)
    {
        // get listing
        $listing = Listing::findOrFail($listing->id);

        // delete listing
        $listing->delete();

        // flash message
        session()->flash('success', 'Listing successfully deleted!');

        // redirect to listing page
        return redirect('/listing');
    }
}
