<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{
    protected $guarded = [];

    // link user and listing
    public function user()
    {
        return $this->belongsTo(User::class, 'submitter_id', 'id');
    }
}
