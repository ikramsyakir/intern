<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

## Demo

Live site: **https://intern.ikramsyakir.me**

## Intern (Interview - Part B)

Developed web-based application using Laravel Framework. This application able to manage user and listing. Besides that, API also can be used to access the data.

## Installation

- Clone the repository with __git clone__
- Copy __.env.example__ file to __.env__ and edit database credentials there
- Run __composer install__
- Run __php artisan key:generate__
- Run __php artisan migrate --seed__ (it has some seeded data for your testing)
- Run __php artisan passport:install__ (API authentication)
- That's it: launch the main URL. 
- Login credentials for admin:- __email: admin@admin.com__ | __password: 123456__

## API EndPoints

All the api endpoints are protected by a simple API Authentication process. To access the resource data, the request header need Accept and Authorization field.

**__Login url__: https://intern.ikramsyakir.me/api/login**

**Example of login API header request**

    curl --location --request POST 'https://intern.ikramsyakir.me/api/login' \
    --header 'Accept: application/json' \
    --header 'Content-Type: multipart/form-data; boundary=--------------------------790647401677586929415471' \
    --form 'email=admin@admin.com' \
    --form 'password=123456'

**Example of response of valid login API**

    {
        "user_id": 2,
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNDU5OWY4YTg4MjJiMGRlYjNkOTJiNDk2YTk0MjYwNjkyNThmNjRmYWY0ZDZkMTE0MTEwZTE0MGIwZjdlOWQxMGMyY2NmYjQ3OTk2ZDBhODQiLCJpYXQiOjE1NzcwNDkyNzksIm5iZiI6MTU3NzA0OTI3OSwiZXhwIjoxNjA4NjcxNjc5LCJzdWIiOiIyIiwic2NvcGVzIjpbXX0.AqjwQ9wVpekY5V_kELw09LxbD4YkP6SMstdD43o5bnaaJhW6BXziCLAJ_fW3JGhf975lzoM4ffRPu8j5TGAYsQ4OTsTcaWOcE9sY0NWIuPz0ViF7UAXsy5VDEvIV3R-wnn91hRYmg_vRsP6qHOaNHCuKbIZI8cbKOnCAJQASZs-pXoicLJH9PBKn_UASlWCKtbllyAMb9xpZQrYCT_YICHpPmX5xauL8C-iCUAZq3MdV6qAMpt9_n76zsJOSE68IazyOALRUeyJRYplLMDBIiIf4Igd__uAMiZsNGmyhVzzveekR3ZeY84sSTIDhe_nRxL_bJGaYTbbtXgAnck85Ul2FKftF9GUeDw_6n6aV0Ed3x38Ru63we3Si1qiMSS0UtHymZhTK-jG25YKZD7kTT78AOeYp76AaqffGamL9cuGrkI9c-JFxPmZsQC4_4P4AJmiK-IFLIMU4z_nU16sfiPha6J5Ji-4zSZ64vWOWwvcnhXFwp4ZVHCxvzv1Mxp2rZZQJNwj-xCkZSLGejOr00Xs_mu0d1MfPVUZDZcgi36jMTUTzD7OHjE3GAj9U4s16cCVt2nq0-fDhil4aW-AflZ8WnqC9mrmKnyQeaDS6HGBcdo6K5e2nVada2vlFzluaGOMvlovkEOvHdk0az7-Oo1YhDBOpaK3jHWwR6tXy1jg",
        "status": {
            "code": 200,
            "message": "Access Granted"
        }
    }

**__Get listing url__: https://intern.ikramsyakir.me/api/{user_id}/{latitude}/{longitude}**

**Example of get listing API header request (Bearer [space] token)**

    curl --location --request GET 'https://intern.ikramsyakir.me/api/listing/2/3.14272/101.25846' \
    --header 'Accept: application/json' \
    --header 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNDU5OWY4YTg4MjJiMGRlYjNkOTJiNDk2YTk0MjYwNjkyNThmNjRmYWY0ZDZkMTE0MTEwZTE0MGIwZjdlOWQxMGMyY2NmYjQ3OTk2ZDBhODQiLCJpYXQiOjE1NzcwNDkyNzksIm5iZiI6MTU3NzA0OTI3OSwiZXhwIjoxNjA4NjcxNjc5LCJzdWIiOiIyIiwic2NvcGVzIjpbXX0.AqjwQ9wVpekY5V_kELw09LxbD4YkP6SMstdD43o5bnaaJhW6BXziCLAJ_fW3JGhf975lzoM4ffRPu8j5TGAYsQ4OTsTcaWOcE9sY0NWIuPz0ViF7UAXsy5VDEvIV3R-wnn91hRYmg_vRsP6qHOaNHCuKbIZI8cbKOnCAJQASZs-pXoicLJH9PBKn_UASlWCKtbllyAMb9xpZQrYCT_YICHpPmX5xauL8C-iCUAZq3MdV6qAMpt9_n76zsJOSE68IazyOALRUeyJRYplLMDBIiIf4Igd__uAMiZsNGmyhVzzveekR3ZeY84sSTIDhe_nRxL_bJGaYTbbtXgAnck85Ul2FKftF9GUeDw_6n6aV0Ed3x38Ru63we3Si1qiMSS0UtHymZhTK-jG25YKZD7kTT78AOeYp76AaqffGamL9cuGrkI9c-JFxPmZsQC4_4P4AJmiK-IFLIMU4z_nU16sfiPha6J5Ji-4zSZ64vWOWwvcnhXFwp4ZVHCxvzv1Mxp2rZZQJNwj-xCkZSLGejOr00Xs_mu0d1MfPVUZDZcgi36jMTUTzD7OHjE3GAj9U4s16cCVt2nq0-fDhil4aW-AflZ8WnqC9mrmKnyQeaDS6HGBcdo6K5e2nVada2vlFzluaGOMvlovkEOvHdk0az7-Oo1YhDBOpaK3jHWwR6tXy1jg'

**Example of response of valid get listing API**

    {
        "listing": [
            {
                "id": 1,
                "list_name": "Starbucks @ Mid Valley Megamall",
                "distance": "46.576"
            },
            {
                "id": 2,
                "list_name": "Ticklish Ribs & Wiches",
                "distance": "47.812"
            },
            {
                "id": 3,
                "list_name": "myBurgerLab Sunway",
                "distance": "38.939"
            },
            {
                "id": 4,
                "list_name": "PappaRich",
                "distance": "37.274"
            }
        ],
        "status": {
            "code": 200,
            "message": "Listing successfully retrieved"
        }
    }

## Part A (Answers)

1. **https://intern.ikramsyakir.me/question1**
2. **https://intern.ikramsyakir.me/question2**
3. **https://intern.ikramsyakir.me/question3**
4. **https://intern.ikramsyakir.me/question4**
5. **https://intern.ikramsyakir.me/question5**
6. **https://intern.ikramsyakir.me/question6**

## Other packages

1. **https://github.com/mjaschen/phpgeo** - to get distance between 2 coordinates

## Theme

1. **https://adminlte.io**

