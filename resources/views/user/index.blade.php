@extends('layouts.adminlte')
@section('title')
    Users
@endsection
@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">

            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif

            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1 class="m-0 text-dark">Users</h1>
                </div><!-- /.col -->
                <div class="col-sm-12 mt-2">
                    <a href="{{ route('users.create') }}" class="btn btn-primary text-white">Add</a>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">List of user</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Type</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->type == 'a' ? 'Administrator' : 'Normal User' }}</td>
                            <td class="text-center">

                                <a href="{{ route('users.edit', $user->id) }}" class="btn btn-primary btn-icon" title="Edit User">
                                    <div><i class="fa fa-edit"></i></div>
                                </a>

                                <button class="btn btn-danger btn-icon" data-id="{{ $user->id }}"data-toggle="modal" data-target="#modaldemo2">
                                    <div><i class="fa fa-trash"></i></div>
                                </button>

                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

        </div>
    </div>
    </section>

    <div id="modaldemo2" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Confirmation</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" id="deleteForm" action="#">
                        <p class="lead">Are you sure you want to delete? </p>
                        <br>
                        @csrf
                        @method('DELETE')
                        <div class="modal-footer justify-content-center">
                            <button type="submit" class="btn btn-danger">Confirm</button>
                            <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
