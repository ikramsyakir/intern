<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('auth.login');
})->middleware('guest');

Auth::routes(['register'=>false]);


Route::get('/home', 'HomeController@index')->name('home');

// Part A Answer

Route::get('/question1', 'PartAController@question1')->name('question1');

Route::get('/question2', 'PartAController@question2')->name('question2');

Route::get('/question3', 'PartAController@question3')->name('question3');

Route::get('/question4', 'PartAController@question4')->name('question4');

Route::get('/question5', 'PartAController@question5')->name('question5');

Route::get('/question6', 'PartAController@question6')->name('question6');

// Part B

Route::resource('/users', 'UserController', ['except' => ['show']])->middleware( 'admin');

Route::resource('/listing', 'ListingController', ['except' => ['show']])->middleware('admin');
